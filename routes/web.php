<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\SensorData;
use Illuminate\Support\Facades\DB;


Route::get('/', function () {
	$data = new SensorData();
	$data = $data->all();
	$luminosity = [];
	$nivel = [];
	$temp = [];
	$labels = [];

	if (sizeof($data) > 30) {
		$i=30;
	}else{
		$i = sizeof($data);
	}

	for ($i; $i > 0; $i--) { 
		$sensor = $data[sizeof($data)-$i];
		array_push($luminosity, $sensor->luminosidade);
		array_push($nivel, $sensor->nivel);
		array_push($temp, $sensor->temp);
		array_push($labels, $sensor->id);
	}

	// foreach ($data->all() as $key => $sensor) {
	// 	array_push($luminosity, $sensor->luminosidade);
	// 	array_push($nivel, $sensor->nivel);
	// 	array_push($temp, $sensor->temp);
	// 	array_push($labels, $sensor->id);
	// }

    return view('welcome', [
		'luminosity' => $luminosity,
		'nivel' => $nivel,
		'temp' => $temp,
		'labels' => $labels
	]);
});

Route::get('/newData', function () {
	$data = new SensorData();
	$data = $data->all();
	$luminosity = [];
	$nivel = [];
	$temp = [];
	$labels = [];

	if (sizeof($data) > 30) {
		$i=30;
	}else{
		$i = sizeof($data);
	}

	for ($i; $i > 0; $i--) { 
		$sensor = $data[sizeof($data)-$i];
		array_push($luminosity, $sensor->luminosidade);
		array_push($nivel, $sensor->nivel);
		array_push($temp, $sensor->temp);
		array_push($labels, $sensor->id);
	}

	return [
		'luminosity' => $luminosity,
		'nivel' => $nivel,
		'temp' => $temp,
		'labels' => $labels
	];
});
