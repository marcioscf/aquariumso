<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\SensorData::class, function (Faker $faker) {
    return [
        'luminosidade' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 10),
        'temp' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 10),
        'nivel' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 10),
    ];
});
