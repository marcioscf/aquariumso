<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <!-- Styles -->
        <style>
            @include('css.css')
        </style>
    </head>
    <body>
        <canvas id="myChart" style="width: 100vw; height:100vh; margin-bottom: 15px;"></canvas>
        <table id="table" class="display" style="width:100%">
             <thead>
                <tr>
                    <th>Id</th>
                    <th>Luminosidade</th>
                    <th>Temperatura</th>
                    <th>Nível da água</th>
                </tr>
            </thead>
            <tbody>
                @foreach($labels as $key=>$id)
                    <tr>
                        <td>{{$id}}</td>
                        <td>{{$luminosity[$key]}}</td>
                        <td>{{$temp[$key]}}</td>
                        <td>{{$nivel[$key]}}</td>
                    </tr>
                @endforeach
            </tbody>
             <tfoot>
                <tr>
                    <th>Id</th>
                    <th>Luminosidade</th>
                    <th>Temperatura</th>
                    <th>Nível da água</th>
                </tr>
            </tfoot>
        </table>
    </body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

<script>
var ctx = document.getElementById("myChart");
var lum = JSON.parse('{{ json_encode($luminosity) }}');
var temp = JSON.parse('{{ json_encode($temp) }}');
var nivel = JSON.parse('{{ json_encode($nivel) }}');
var labels = JSON.parse('{{ json_encode($labels) }}');

var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: labels,
        datasets: [{
            type: 'line',
            label: 'Temperatura',
            data: temp,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
                'rgba(255,99,132,1)',
            ],
            borderWidth: 1
        },
        {
            type: 'line',
            label: 'Luminosidade',
            data: lum,
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)',
            ],
            borderWidth: 1
        },
        {
            type: 'line',
            label: 'Nível',
            data: nivel,
            backgroundColor: [
                'rgba(255, 206, 86, 0.2)',
            ],
            borderColor: [
                'rgba(255, 206, 86, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

datatbl = $('#table').DataTable(
    {
        "dom": '<"dt-buttons"Bf><"clear">lirtp',
        "paging": true,
        "autoWidth": true,
        "buttons": [
            'colvis',
            'copyHtml5',
    'csvHtml5',
            'excelHtml5',
    'pdfHtml5',
            'print'
        ],
        "columns": [
            { "data": "id" },
            { "data": "temp" },
            { "data": "lum" },
            { "data": "niv" }
        ]
    }
);

function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;

  // If you don't care about the order of the elements inside
  // the array, you should sort both arrays here.

  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}

setInterval(function() {
    $.get('{{asset('newData')}}', function(data, status){
        if (!arraysEqual(labels, data.labels)){
            labels = data.labels;
            
            myChart.data.labels = data.labels
            myChart.data.datasets[0].data = data.temp;
            myChart.data.datasets[1].data = data.luminosity;
            myChart.data.datasets[2].data = data.nivel;
            myChart.update();

            datatbl.clear();
        
            $.each(data.labels, function( index, value ) {
                datatbl.rows.add([
                    {
                    'id':data.labels[index],
                    'lum':data.luminosity[index],
                    'temp':data.temp[index],
                    'niv':data.nivel[index]}
                ])
            });

            datatbl.columns.adjust().draw();
        }
    });
}, 500);
</script>
